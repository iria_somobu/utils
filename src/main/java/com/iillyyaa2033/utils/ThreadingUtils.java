package com.iillyyaa2033.utils;

public class ThreadingUtils {

    public static class MyLock {

        private volatile boolean isLocked = false;

        public boolean isLocked() {
            return isLocked;
        }

        public synchronized void lock() {
            while (isLocked) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            isLocked = true;
        }

        public synchronized void lockIfUnlocked(){
            isLocked = true;
        }

        public synchronized void waitUnlock() {
            while (isLocked) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public synchronized void unlock() {
            isLocked = false;
            notifyAll();
        }
    }

}
